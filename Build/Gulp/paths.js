export const css = {
    // Add additional glob-paths as needed for watcher
    source: [
        'Resources/Private/Styles/**/*.scss',
    ],
    includePaths: 'node_modules/bootstrap/scss',
    dest: 'Resources/Public/Styles'
};

export const js = {
    // Add additional glob-paths as needed for watcher
    source: [
        'Resources/Private/JavaScript/**/*.js',
    ],
    dest: 'Resources/Public/JavaScript',
    filename: 'main.min.js'
};