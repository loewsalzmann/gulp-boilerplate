import {js as paths} from './paths';

const cwd = process.cwd();
const gulp = require("gulp");
const esLint = require('gulp-eslint');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');

export const compileJs = () => {
    return gulp.src(paths.source, {cwd: cwd})
        .pipe(esLint('./.eslintrc.json'))
        .pipe(esLint.format())
        .pipe(babel())
        .pipe(concat(paths.filename))
        .pipe(uglify())
        .pipe(gulp.dest(paths.dest))
};

