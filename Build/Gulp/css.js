import { css as paths } from './paths';

const path = require('path');
const gulp = require('gulp');
const postCSS = require('gulp-postcss');
const sass = require('gulp-sass');
const sassGlob = require('gulp-sass-glob');
const sassLint = require('gulp-sass-lint');
const cwd = process.cwd();

export const styles = () => {
    return gulp.src(paths.source, {cwd: cwd})
        .pipe(sassGlob())
        .pipe(sass({
            includePaths: paths.includePaths
        }))
        .pipe(postCSS())
        .pipe(gulp.dest(path.join(cwd, paths.dest)));
}

export const styleLint = () => {
    return gulp.src(paths.source, {cwd: cwd})
        .pipe(sassGlob())
        .pipe(sassLint({
            configFile: '.sass-lint.yaml',
            endless: true
        }))
        .pipe(sassLint.format());
}
