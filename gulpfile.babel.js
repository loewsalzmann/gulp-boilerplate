import gulp from 'gulp';
import {styles, styleLint} from './Build/Gulp/css';
import {compileJs} from './Build/Gulp/scripts';
import * as paths from './Build/Gulp/paths';

function watchFiles() {
    gulp.watch(paths.css.source, gulp.series(styleLint, styles));
    gulp.watch(paths.js.source, compileJs);
}

gulp.task('css', gulp.series(styleLint, styles));
gulp.task('scripts', gulp.series(compileJs));
gulp.task('default', gulp.parallel(gulp.series(styleLint, styles), gulp.series(compileJs)));

gulp.task('watch', gulp.parallel(watchFiles));
